<?php

/**
 * Service endpoint to request a relationship.
 */
function _user_relationship_resource_request($uid, $name) {
	$type = user_relationships_type_load(array('name' => $name));

	global $user;
	$relationship = user_relationships_request_relationship($user->uid, $uid, $type);
	if (!$relationship) {
		return service_error(t('Relationship request is not allowed.'), 401);
	}
	return $relationship;
}

/**
 * Service endpoint to delete a relationship.
 */
function _user_relationship_resource_delete($rid, $action = 'remove') {
	$relationships = user_relationships_load(array('rid' => $rid));
	$relationship = array_pop($relationships);

	global $user;

	user_relationships_delete_relationship($relationship, $user, $action);
	return TRUE;
}

/**
 * Service endpoint to approve a relationship.
 */
function _user_relationship_resource_approve($rid) {
	$relationships = user_relationships_load(array('rid' => $rid));
	$relationship = array_pop($relationships);

	global $user;

	$updated_relationship = user_relationships_save_relationship($relationship, 'approve');
	return $updated_relationship;
}
	
/**
 * Service endpoint to get all relationships for a user.
 */
function _user_relationship_resource_retrieve($uid) {
	$relationships = user_relationships_load(array('user' => $uid));
	return $relationships;
}
